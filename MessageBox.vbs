' Process the first command line argument as the message to display.
Set objArgs = WScript.Arguments
messageText = objArgs(0)

' Display in console
WScript.Echo messageText

' Prompt for how to continue.
' Using Stdout.Write prevents trailing newline, per https://www.winvistatips.com/threads/wscript-echo-without-a-carriage-return.752350/
' Doing it that way so can erase this line below.
WScript.Stdout.Write "Close message box to continue..."

' Display using message box
MsgBox messageText

' Erase prompt for how to continue by sending carriage return then overwriting with spaces.
WScript.Stdout.Write vbCr & "                                  "
