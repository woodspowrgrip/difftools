@echo off

rem This batch file checks whether the first two arguments refer to the same
rem file.  If the args match exactly, returns 1.  If they are different but
rem the absolute path of the file they refer to is the same, returns 2.  Else 
rem returns 0.  Test cases:
rem 	AbsolutePathOfFilesIsTheSame.bat temp temp2               -> returns 0
rem 	AbsolutePathOfFilesIsTheSame.bat temp temp                -> returns 1
rem 	AbsolutePathOfFilesIsTheSame.bat temp ..\DiffTools\temp   -> returns 2

rem The pushd/popd stuff makes this work on the current directory even if run from a UNC path.
rem From http://stackoverflow.com/questions/9013941/how-to-run-batch-file-from-network-share-without-unc-path-are-not-supported-me
pushd %~dp0

rem Check for exactly 2 args (%2 not empty but %3 is).
if not "%~2"=="" if "%~3"=="" goto ARG_COUNT_OK
echo This batch file requires two arguments, the filenames to check to see if they refer to the same file.

echo Got the following:
echo Argument 1: %1
echo Argument 2: %2
echo Argument 3: %3

timeout -1 1>NUL
goto :EOF
:ARG_COUNT_OK

rem Check whether arg1 and arg2 are literally the same.
rem Could use a case insensive comparison here or make a separate case for that below.
if %1==%2 (
	echo.
	echo %0: Arg1 and Arg2 are the same.
	
	popd
	exit /B 1
) else (
	rem echo.
	rem echo The arguments are not the same, but may refer to the same file.  Checking that...
)

rem Check whether arg1 and arg2 resolve to the same absolute path even though they aren't literally 
rem the same.
set absolute_path_of_arg_1=%~dpnx1
set absolute_path_of_arg_2=%~dpnx2
rem When deferencing the absolute paths below, using percent characters (e.g. %absolute_path_of_arg_1%)
rem can cause the batch file to exit unexpectedly with some paths (such as Fork sometimes generates).  
rem Enabling delayed expansion and using exclamation points (e.g. !absolute_path_of_arg_1!) instead 
rem seems to solve that problem.  For more info about that see 
rem https://stackoverflow.com/questions/14954271/string-comparison-in-batch-file 
SetLocal EnableDelayedExpansion

if !absolute_path_of_arg_1!==!absolute_path_of_arg_2! (
	echo.
	echo %0: Even though the args are different, they resolve to the same absolute file path:
	echo.
	echo     Argument 1: %1
	echo     Argument 2: %2
	echo.
	rem Need to use delayed expansion for the following two lines.
	echo     Absolute path of argument 1:  !absolute_path_of_arg_1!
	echo     Absolute path of argument 2:  !absolute_path_of_arg_2!
	popd
	exit /B 2
) else (
	rem echo.
	rem echo The arguments refer to different files.
)

:EOF

rem Also need to do this before each call to "exit" above.
popd

rem The following line seems to change the directory to the one containing the batch file
rem when it is too late to fix with popd.  Can deal with that from the calling batch file 
rem using "pushd .\" before calling this and "popd" after returning (and testing the ERRORLEVEL).
exit /B 0
