## Purpose

This collection of batch and VBS files makes it easy to use different tools to compare different types of files (distinguished by the filename extension).  You can just run AutoSelectDiffTool.bat followed by the two filenames and it will figure out which tool to use.  This is especially useful when configuring the external diff tool for a version control GUI.

For example, you might normally use Fork (from git-fork.com) to compare source code using p4merge.exe, but occasionally want to compare Microsoft Excel (XLS) files using a different tool.  This project contains everything needed to do that by automatically using Excel to export to CSV, which can then be compared with p4merge.exe.  It can be easily modified to support other file types and comparison tools.  Pull requests with enhancements are welcome.

As a time saving measure, it also checks for some trivial cases:

1. Same filename specified twice (there is no point in comparing a file with itself).
2. The filenames specified as arguments different but refer to the same file in the end (they have the same absolute path).
3. The arguments refer to different files that have identical contents.

---

## Usage

**Command line**

AutoSelectDiffTool.bat <filename1> <filename2>

** Fork **

File menu > Preferences > Integration tab.  Under External Diff Tool, set as follows:

1. Diff Tool: Custom
2. Diff Tool Path: <browse to and select AutoSelectDiffTool.bat>
3. Arguments: "$REMOTE" "$LOCAL"
