@echo off

rem This batch file generally calls p4merge on the specified files.
rem But in special cases (detected by filename extension) it can do
rem more specialized comparisons.

rem Possible improvements:
rem - Make it compare zip file metadata.  Find a way to print the contents 
rem   of a zip including a hash for each file it contains, so can compare 
rem   contents.
rem   - Or better yet, extract zip files into temporary folders and do a 
rem     folder compare with Meld or p4merge.
rem - Make it easy to call this from Windows Explorer (it is currently 
rem   used mostly from a version control GUI like Git Fork).  Info about
rem   doing this can be found here:
rem   https://superuser.com/questions/110996/windows-explorer-diff-two-files-from-the-context-menu
rem   https://stackoverflow.com/questions/13752998/compare-two-files-in-visual-studio/45714611#45714611

rem Check for exactly 2 args (%2 not empty but %3 is).
rem Putting two "if" in a statement works like a logical AND.
if not "%~2"=="" if "%~3"=="" goto ARG_COUNT_OK
echo This batch file requires two arguments, the names of the files to compare.

echo Got the following (some may not have been specified):
echo First argument:  %1
echo Second argument: %2
echo Third argument:  %3

timeout -1 1>NUL
goto :EOF
:ARG_COUNT_OK

rem Use this when calling another batch file in the same folder as this one.
rem May not be necessary; the root issue might have been something else.
set folder_of_running_batch_file=%~dp0

echo Checking for the trivial case of comparing a file with itself...

rem This "call" changes the folder to the one containing the batch files when it 
rem executes "exit" to set the return value.  Using "cmd /c" rather than "call" 
rem here might work around that problem, but it doesn't seem to set ERRORLEVEL 
rem as desired.  Working around those things with pushd/popd.

pushd .\
call %folder_of_running_batch_file%AbsolutePathOfFilesIsTheSame.bat %1 %2
if %ERRORLEVEL% NEQ 0 (
	popd
	cscript %folder_of_running_batch_file%MessageBox.vbs "Comparing a file with itself isn't useful.  Check arguments."
	goto :EOF
) else ( popd )

echo Checking for the trivial case of the files being identical...
fc %1 %2 > nul
if %ERRORLEVEL% equ 0 (
	cscript %folder_of_running_batch_file%MessageBox.vbs "The files are identical."
	rem Not sure if we want the following goto or not.  With it disabled by "rem" you can view the identical CSV files.
	goto EOF
)

echo Checking for special cases which use different methods for doing the comparison...
set extension_of_arg_1=%~x1
echo Extension of first argument: %extension_of_arg_1%

rem Checking for *.xls.
if not "%extension_of_arg_1%"==".xls" if not "%extension_of_arg_1%"==".XLS" goto NOT_XLS
echo "Special handling for comparing .xls files..."
echo folder_of_running_batch_file %folder_of_running_batch_file%
call %folder_of_running_batch_file%DiffExcelFilesUsingCsvExport.bat %1 %2
goto :EOF
:NOT_XLS

rem Checking for *.xlsx.
if not "%extension_of_arg_1%"==".xlsx" if not "%extension_of_arg_1%"==".XLSX" goto NOT_XLSX
echo "Special handling for comparing .xlsx files..."
echo folder_of_running_batch_file %folder_of_running_batch_file%
call %folder_of_running_batch_file%DiffExcelFilesUsingCsvExport.bat %1 %2
goto :EOF
:NOT_XLSX

rem Checking for *.out (TI MCU executables).
if not "%extension_of_arg_1%"==".out" if not "%extension_of_arg_1%"==".OUT" goto NOT_OUT
echo "Special handling for comparing .out files..."
rem "fc" just does command line output, but it does try to make sense of the file contents.
rem fc %1 %2
rem "fc /b" just does command line output.  Prints a list of addresses and the difference in value at each address.
rem fc /b %1 %2
rem "comp" just does command line output.  Prints a list of addresses and the difference in value at each address.  The above is formatted more nicely.
rem comp %1 %2
rem "VBinDiff" is well-liked.  Open source.  Console but graphical.  I didn't try it.  
rem   Homepage:  https://web.archive.org/web/20151118022528/http://www.cjmweb.net/vbindiff/
rem   Github:  https://github.com/madsen/vbindiff
rem   Updated Windows build:  https://github.com/mkoloberdin/vbindiff/releases/tag/efbf1e3 - trying this
%folder_of_running_batch_file%vbindiff.exe %1 %2
rem Haven't tried: WinMerge looks nice.  Not sure what it does if file sizes are different.  https://manual.winmerge.org/en/Compare_bin.html
rem Haven't tried: "HxD" might be good for files of the same size.  https://mh-nexus.de/en/hxd/.  Reportedly handles huge files, but shows every byte as different after an inserted byte.
rem Haven't tried: Guiffy Binary Diff Tool.  https://www.guiffy.com/Binary-Diff-Tool.html  50MB because it includes an isolated Java JRE.
rem Haven't tried: Araxis Merge looks nice, but isn't free.  https://www.araxis.com/merge/documentation-windows/comparing-binary-files.en
rem Haven't tried: Shelwien/cmp might be good, but it isn't at all common.  https://github.com/Shelwien/cmp/
rem Haven't tried: ECMerge costs $30 to $50.
goto :EOF
:NOT_OUT

rem Default case.  Change the below line to p4merge, meld, or ask (which prompts each time).  Or comment it for default of p4merge.
set defaultComparisonTool=ask
rem If set to ask, do that.
if "%defaultComparisonTool%"=="ask" (
	choice /c pm /n /m "Press 'p' to compare using p4merge or press 'm' to use meld."
	if errorlevel 1 set defaultComparisonTool=p4merge
	if errorlevel 2 set defaultComparisonTool=meld
)
rem Call the diff tool.
call MinimizeConsoleWindow.bat
echo 
echo 
if "%defaultComparisonTool%"=="meld" (
	echo "Default case.  Calling meld."
	"C:\Program Files\Meld\Meld.exe" %1 %2
) else (
	echo "Default case.  Calling p4merge."
	"C:\Program Files\Perforce\p4merge.exe" %1 %2
)

:EOF

rem pause
