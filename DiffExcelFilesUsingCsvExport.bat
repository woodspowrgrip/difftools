@echo off

rem This batch file facilitates comparing .xls (Microsoft Excel)
rem files.  It first uses Excel to export each file to .csv, then
rem calls p4merge on those files.

rem XlsToCsv needs complete path names for the input and output files.
rem Must do this before pushd, at least if running this from command line on mapped network drive.
set absolute_path_of_arg_1=%~dpnx1
set absolute_path_of_arg_2=%~dpnx2

rem The pushd/popd stuff makes this work on the current directory even if run from a UNC path.
rem From http://stackoverflow.com/questions/9013941/how-to-run-batch-file-from-network-share-without-unc-path-are-not-supported-me
pushd %~dp0

rem Check for exactly 2 args (%2 not empty but %3 is).
if not "%~2"=="" if "%~3"=="" goto ARG_COUNT_OK
echo This batch file requires two arguments, the names of the files to compare.

echo Got the following:
echo Argument 1: %1
echo Argument 2: %2
echo Argument 3: %3

timeout -1 1>NUL
goto :EOF
:ARG_COUNT_OK

rem Delete temp files so they won't exist unless they get created below.
rem Check whether they exist before deleting to avoid error messages.
if exist "%CD%\temp_arg_1.csv" del "%CD%\temp_arg_1.csv"
if exist "%CD%\temp_arg_2.csv" del "%CD%\temp_arg_2.csv"

rem Exporting CSV files using Excel.
rem
rem It is a VBS script that drives Excel, so the output exactly matches a manual export.
echo Converting argument 1 to CSV:  %absolute_path_of_arg_1%
rem %CD% makes it use the current directory: https://blogs.msdn.microsoft.com/oldnewthing/20050128-00/?p=36573
XlsToCsv.vbs "%absolute_path_of_arg_1%" "%CD%\temp_arg_1.csv"
rem Now do the second arg
echo Converting argument 2 to CSV:  %absolute_path_of_arg_2%
XlsToCsv.vbs "%absolute_path_of_arg_2%" "%CD%\temp_arg_2.csv"

rem Check to see if the temporary CSV files are identical.
fc "%CD%\temp_arg_1.csv" "%CD%\temp_arg_2.csv" > nul
if %ERRORLEVEL% equ 0 (
	call cscript MessageBox.vbs "The CSV exports of the (first sheet of the) Excel files are identical (but formatting and other sheets may not be)."
	rem Not sure if we want the following goto or not.  With it disabled by "rem" you can view the CSV files.
	goto EOF
)

rem Now compare the temporary CSV files.
call MinimizeConsoleWindow.bat
"C:\Program Files\Perforce\p4merge.exe" "%CD%\temp_arg_1.csv" "%CD%\temp_arg_2.csv"

:EOF

popd
