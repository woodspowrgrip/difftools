@echo off

rem Minimizes the console window.
rem From https://stackoverflow.com/questions/9232308/how-do-i-minimize-the-command-prompt-from-my-bat-file
rem Uses  windowMode from https://github.com/npocmaka/batch.scripts/blob/master/hybrids/.net/c/windowMode.bat
rem Uses getCmdPid.bat from https://github.com/npocmaka/batch.scripts/blob/master/hybrids/.net/getCmdPID.bat

call getCmdPid
call windowMode -pid %errorlevel% -mode minimized