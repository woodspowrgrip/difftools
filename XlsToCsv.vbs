Dim xlsFilename
Dim csvFilename
xlsFilename = WScript.Arguments.Item ( 0 )
csvFilename = WScript.Arguments.Item ( 1 )
'WScript.Echo "Arg0: "+ xlsFilename
'WScript.Echo "Arg1: " + csvFilename

If WScript.Arguments.Count = 2 Then
	'WScript.Echo Wscript.Arguments.Item ( 2 )
	Call ExportXlsToCvsFile( xlsFilename, csvFilename )
Else
    WScript.Echo "Error! Please specify the source path and the destination. Usage: XlsToCsv SourcePath.xls Destination.csv"
End If
    Wscript.Quit


'///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
' 	SUBS
'///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Function DeleteFile(filePath)
    On Error Resume Next

    Dim objFSO
    Set objFSO = CreateObject("Scripting.FileSystemObject")
    
    If objFSO.FileExists(filePath) Then
        objFSO.DeleteFile(filePath)
        If Err.Number = 0 Then
            DeleteFile = True ' File deleted successfully
        Else
            DeleteFile = False ' Error occurred while deleting file
        End If
    Else
        DeleteFile = False ' File doesn't exist
    End If
    
    Set objFSO = Nothing
    On Error GoTo 0
End Function


Function DeleteFileAndReportErrors( filePath )
    Dim objFSO
    Set objFSO = CreateObject("Scripting.FileSystemObject")
    If objFSO.FileExists(filePath) Then

		deleted = DeleteFile( filePath )

		If deleted Then
			MsgBox "File " & filePath & " deleted successfully."
		Else
			MsgBox "Error occurred while deleting file: " & filePath
		End If	
		
	Else
			MsgBox "Can't delete file that doesn't exist: " & filePath
	End If

End Function

Function GetTempFilePath()
    Dim objFSO, tempFileName, tempFilePath
    
    Set objFSO = CreateObject("Scripting.FileSystemObject")
    
    tempFileName = objFSO.GetTempName()
    tempFilePath = objFSO.GetSpecialFolder(2) & "\" & tempFileName
    
    Set objFSO = Nothing
    
    GetTempFilePath = tempFilePath
End Function


Function AppendFile(sourceFilePath, destinationFilePath)
    On Error Resume Next

    Dim objFSO, objSourceFile, objDestinationFile
    Set objFSO = CreateObject("Scripting.FileSystemObject")

    ' Open the source file for reading in binary mode
    Set objSourceFile = objFSO.OpenTextFile(sourceFilePath, 1, False, -1) ' -1 represents TristateTrue (binary mode)

    ' Open the destination file for appending in binary mode
    Set objDestinationFile = objFSO.OpenTextFile(destinationFilePath, 8, True, -1) ' -1 represents TristateTrue (binary mode)

    ' Read the contents of the source file and append to the destination file
    objDestinationFile.Write objSourceFile.ReadAll

    ' Close the files
    objSourceFile.Close
    objDestinationFile.Close

    If Err.Number = 0 Then
        AppendFile = True ' File appended successfully
    Else
        AppendFile = False ' Error occurred while appending
    End If

    On Error GoTo 0
End Function



Function AppendStringToFile(filePath, appendString)
    On Error Resume Next

    Dim objFSO, objFile
    Set objFSO = CreateObject("Scripting.FileSystemObject")

    ' Open the file in append mode
    Set objFile = objFSO.OpenTextFile(filePath, 8, True) ' 8 represents ForAppending

    ' Append the string to the file
    objFile.Write appendString

    ' Close the file
    objFile.Close

    Set objFile = Nothing
    Set objFSO = Nothing

    If Err.Number = 0 Then
        AppendStringToFile = True ' String appended successfully
    Else
        AppendStringToFile = False ' Error occurred while appending
    End If

    On Error GoTo 0
End Function


Function ExportXlsToCvsFile( xlsFilename, csvFilename )
	' File names for symbol processing
	Dim unicode_temp_file_name
	Dim csv_file_name
	Dim oExcel
	Set oExcel = CreateObject("Excel.Application")
	Dim oBook
	Set oBook = oExcel.Workbooks.Open( xlsFilename )
	Dim outputFilename
	outputFilename = csvFilename
	DeleteFile( outputFilename )									' Delete the file so can just append to it with the contents of each worksheet (no special case code for the first one).

	Dim tempFilePath
	
	' Loop through each worksheet
	Dim index
	index = 1
	
	For Each objWorksheet In oBook.Worksheets
		' Save the worksheet to temp file as CSV
		tempFilePath = GetTempFilePath()							' We need a new temp file for each worksheet because Excel doesn't let go of the old one right away.
		objWorksheet.SaveAs tempFilePath, 6 						' 6 represents CSV file format
		
		' Insert a delimiter between the output from each worksheet
		AppendStringToFile outputFilename, "=================== Worksheet " & CStr(index) & " ===============================" & vbCrLf & vbCrLf
			
		' Append the temp file to the output file.
		appended = AppendFile(tempFilePath, outputFilename)
		
		If appended Then
			'MsgBox "File appended successfully."
		Else
			MsgBox "Error occurred while appending file " & outputFilename
		End If	

		AppendStringToFile outputFilename, vbCrLf & vbCrLf
		
		' Cleaning up the temp files doesn't seem to work.  Maybe Excel doesn't let go
		' soon enough.  Waiting until after calling oExcel.Quit doesn't help.  For now just
		' letting a bunch of temp files accumulate in e.g. C:\Users\dawsonb\AppData\Local\Temp
		' DeleteFileAndReportErrors( tempFilePath )					' Clean up the temp file

		index = index + 1
	Next

	oBook.Close False
	oExcel.Quit

	
End Function
